/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function() {   
    $('#datepicker').datepicker();
    
    $(".loginBtn").click(function(evt){
        $(".errorAlert").show();
        var email = $(".emailInput").val();
        var password = $(".passwordInput").val();
        if(email === "") {
            $(".errorLoginMessage").html('Email can not be empty.');
            $(".errorAlert").show();
            evt.preventDefault();
        }else if(password === "") {
            $(".errorLoginMessage").html('Password can not be empty.');
            $(".errorAlert").show();
            evt.preventDefault();
        }else {
            $(".errorAlert").hide();
        }
    });
    
    $(".signUpBtn").click(function(evt) {
        var name = $(".nameInput").val();
        var email = $(".emailInput").val();
        var phone = $(".phoneInput").val();
        var password = $(".passwordInput").val();
        var rePassword = $(".rePasswordInput").val();
        function isValidPhoneNumber(phone) {
            var pattern = new RegExp(/^[0-9]*$/);
            return pattern.test(phone);
        }
        
        function isValidEmail(email) {
            var pattern = new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/);
            return pattern.test(email);
        }
        
        function isValidPassword(password) {
            var pattern = new RegExp(/^(?=.*\d).{4,8}$/);
            return pattern.test(password);
        }
        
        if(email === "") {
            $(".signUpErrorMessage").html('Email can not be empty.');
            evt.preventDefault();
        }else if(phone === "") {
            $(".signUpErrorMessage").html('Phone number can not be empty.');
            evt.preventDefault();
        }else if(name === "") {
            $(".signUpErrorMessage").html('Name can not be empty.');
            evt.preventDefault();
        }else if(password === "") {
            $(".signUpErrorMessage").html('Password can not be empty.');
            evt.preventDefault();
        }else if(password !== rePassword) {
            $(".signUpErrorMessage").html('Passwords do not match.');
            evt.preventDefault();
        }else if(!isValidPhoneNumber(phone)) {
            $(".signUpErrorMessage").html('Invalid phone number format.');
            evt.preventDefault();
        }else if(!isValidEmail(email)) {
            $(".signUpErrorMessage").html('Invalid email format.');
            evt.preventDefault();
        }else if(!isValidPassword(password)) {
            $(".signUpErrorMessage").html('Password must be between 4 and 8 digits long and include at least one numeric digit.');
            evt.preventDefault();
        }else {
            $(".errorAlert").hide();
        }
    });
    
    $(".updateBtn").click(function(evt) {
        var title = $(".bookTitleInput").val();
        var author = $(".bookAuthorInput").val();
        var publisher = $(".bookPublisherInput").val();
        var isbn = $(".bookIsbnInput").val();
        var desc = $(".bookDescInput").val();
        
        if(title === "") {
            $(".editBookErrorMessage").html('Please enter the book title.');
            evt.preventDefault();
        }else if(author === "") {
            $(".editBookErrorMessage").html('Please enter the book author.');
            evt.preventDefault();
        }else if(publisher === "") {
            $(".editBookErrorMessage").html('Please enter the book publisher.');
            evt.preventDefault();
        }else if(isbn === "") {
            $(".editBookErrorMessage").html('Please enter the book ISBN.');
            evt.preventDefault();
        }else if(desc === "") {
            $(".editBookErrorMessage").html('Please enter the book descrpition.');
            evt.preventDefault();
        }else {
            $(".errorAlert").hide();
        }
    });
    
    $(".createBtn").click(function(evt) {
        var title = $(".bookTitleInput").val();
        var author = $(".bookAuthorInput").val();
        var publisher = $(".bookPublisherInput").val();
        var isbn = $(".bookIsbnInput").val();
        var desc = $(".bookDescInput").val();
        
        if(title === "") {
            $(".editBookErrorMessage").html('Please enter the book title.');
            evt.preventDefault();
        }else if(author === "") {
            $(".editBookErrorMessage").html('Please enter the book author.');
            evt.preventDefault();
        }else if(publisher === "") {
            $(".editBookErrorMessage").html('Please enter the book publisher.');
            evt.preventDefault();
        }else if(isbn === "") {
            $(".editBookErrorMessage").html('Please enter the book ISBN.');
            evt.preventDefault();
        }else if(desc === "") {
            $(".editBookErrorMessage").html('Please enter the book descrpition.');
            evt.preventDefault();
        }else {
            $(".errorAlert").hide();
        }
    });
    
    

});

