/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.security.MessageDigest;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import monashbook.ejb.BookService;
import monashbook.models.Loan;
import monashbook.models.Patron;
import monashbook.models.Review;

/**
 *
 * @author Liang
 */
@Named(value = "loginController")
@SessionScoped
public class LoginController implements Serializable {
    @EJB
    BookService bookService;
    private List<Patron> patrons;
    private Patron loginedPatron;
    private String email;
    private String password;
    private String hashedPassword;
    private String rePassword;
    private String name;
    private String phone;
    private String errorMessage;
    private String signUpErrorMessage;
    public LoginController() {
        
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }
    
    public String getRePassword() {
        return rePassword;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getSignUpErrorMessage() {
        return signUpErrorMessage;
    }

    public void setSignUpErrorMessage(String signUpErrorMessage) {
        this.signUpErrorMessage = signUpErrorMessage;
    }
    
    public Patron getLoginedPatron() {
        return loginedPatron;
    }

    public void setLoginedPatron(Patron loginedPatron) {
        this.loginedPatron = loginedPatron;
    }
    
    public List<Loan> getPatronLoans() {
        return loginedPatron.getLoans();
    }
    
    public List<Patron> getPatrons() {
        return bookService.getAllPatrons();
    }
    
    public List<Review> getPatronReviews() {
        return loginedPatron.getReviews();
    }
    
    public String loginClick() {
        loginedPatron = new Patron();
        patrons = bookService.getAllPatrons();
        String url = "index.xhtml";
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getSessionMap().put("email", email);
        patrons = bookService.getAllPatrons();
        // Hash the input password into SHA-256 format
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());
            byte byteData[] = md.digest();
 
            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
             sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
             hashedPassword = sb.toString();
            }
        }
        catch(Exception e) {
            System.out.println("MessageDigest Error.");
        }
        
        for(Patron p:patrons){
            if(p.getEmail().equals(email) && p.getPassword().equals(hashedPassword)) {
                loginedPatron = p;
                errorMessage = "You have already logined.";
                url = "index.xhtml";
            }else if(p.getEmail().equals(email) && !p.getPassword().equals(hashedPassword)){
                loginedPatron = null;
                errorMessage = "Invalid password, please re-enter.";
                url = "login.xhtml";
            }else if(p.getPassword().equals(hashedPassword) && !p.getEmail().equals(email)) {
                loginedPatron = null;
                errorMessage = "Invalid email, please re-enter.";
                url = "login.xhtml";
            }else {
                loginedPatron = null;
                errorMessage = "Invalid email and password, please re-enter.";
                url = "login.xhtml";
            }
        }
        return url;
    }
    public String signUp() {
        // Hash the input password into SHA-256 format
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(password.getBytes());
            byte byteData[] = md.digest();
 
            //convert the byte to hex format method 1
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
             sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
             hashedPassword = sb.toString();
            }
        }
        catch(Exception e) {
            System.out.println("MessageDigest Error.");
        }
        
        
        String url = "index.xhtml";
        if(loginedPatron == null){
            loginedPatron = new Patron();
            if(name.equals("")){
                signUpErrorMessage = "Name can not be empty.";
                url = "registration.xhtml";
            }else if(email.equals("")) {
                signUpErrorMessage = "Email can not be empty.";
                url = "registration.xhtml";
            }else if(phone.equals("")) {
                signUpErrorMessage = "Phone number can not be empty.";
                url = "registration.xhtml";
            }else if(password.equals("")) {
                signUpErrorMessage = "Password can not be empty.";
                url = "registration.xhtml";
            }else if(!password.equals(rePassword)) {
                signUpErrorMessage = "Passwords do not match.";
                url = "registration.xhtml";
            }else {
                loginedPatron.setPassword(hashedPassword);
                loginedPatron.setEmail(email);
                loginedPatron.setName(name);
                loginedPatron.setPhone(phone);
                bookService.addPatron(loginedPatron);
                errorMessage = "You have already logined.";
                url = "index.xhtml";
            }
        }
        return url;
    }
}
