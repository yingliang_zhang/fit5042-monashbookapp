/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import monashbook.ejb.BookService;
import monashbook.models.Book;
import monashbook.models.Loan;
import monashbook.models.Patron;

/**
 *
 * @author Liang
 */
@Named(value = "loanController")
@SessionScoped
public class LoanController implements Serializable {
    @EJB
    BookService bookService;
    private Book book;
    private Patron patron;
    private List<Book> books;
    /**
     * Creates a new instance of LoanController
     */
    public LoanController() {
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
    
    public void makeLoan() {
        FacesContext fc = FacesContext.getCurrentInstance();
        LoginController loginController = (LoginController) fc.getELContext().getELResolver().getValue(fc.getELContext(), null, "loginController");
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String isbn = params.get("isbn");
        books = new ArrayList<>();
        book = new Book();
        books = bookService.getAllBooks();
        for(int i = 0; i < books.size(); i++) {
            System.out.println(books.get(i).getAvailability());
            if(books.get(i).getIsbn().equals(isbn)) {
                book = books.get(i);
            }
        }
        patron = loginController.getLoginedPatron();
        if(patron == null) {
            loginController.setErrorMessage("Please login to make a loan.");
        }else {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            Date date = new Date();
            String currentDate = dateFormat.format(date);
            Loan loan = new Loan();
            loan.setPatron(patron);
            loan.setBook(book);
            loan.setLoanDate(currentDate);
            loan.setDueDate(currentDate);
            loan.setStatus("In Progress...");
            book.getLoans().add(loan);
            book.setAvailability(false);
            loginController.getPatronLoans().add(loan);
            //bookService.addLoan(loan);
            bookService.addBook(book);
        }
    }
    
    public void bookmarkBook() {
        FacesContext fc = FacesContext.getCurrentInstance();
        LoginController loginController = (LoginController) fc.getELContext().getELResolver().getValue(fc.getELContext(), null, "loginController");
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String isbn = params.get("isbn");
        books = new ArrayList<>();
        book = new Book();
        books = bookService.getAllBooks();
        for(int i = 0; i < books.size(); i++) {
            System.out.println(books.get(i).getAvailability());
            if(books.get(i).getIsbn().equals(isbn)) {
                book = books.get(i);
            }
        }
        patron = loginController.getLoginedPatron();
        if(patron == null) {
            loginController.setErrorMessage("Please login to mark a book.");
        }else {
            patron.getMarkedBooks().add(book);
            bookService.addPatron(patron);
        }
        
        
    }
}
