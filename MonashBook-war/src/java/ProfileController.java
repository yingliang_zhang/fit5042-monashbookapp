/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import monashbook.ejb.BookService;
import monashbook.models.Book;
import monashbook.models.Loan;
import monashbook.models.Patron;
import monashbook.models.Review;

/**
 *
 * @author Liang
 */
@Named(value = "profileController")
@SessionScoped
public class ProfileController implements Serializable {
    @EJB
    BookService bookService;
    private Book book;
    private Patron patron;
    private List<Book> loanedBooks;
    private List<Book> markedBooks;
    private List<Review> patronReviews;
    private List<Loan> loans;
    private List<Loan> theBookLoans;
    private Loan loan;
    private String bookmarkMessage;
    private String loanMessage;
    private String reviewMessage;
    /**
     * Creates a new instance of ProfileController
     */
    public ProfileController() {
    }

    public List<Book> getMarkedBooks() {
        return markedBooks;
    }

    public void setMarkedBooks(List<Book> markedBooks) {
        this.markedBooks = markedBooks;
    }

    public Patron getPatron() {
        return patron;
    }

    public void setPatron(Patron patron) {
        this.patron = patron;
    }

    public List<Loan> getLoans() {
        return loans;
    }

    public void setLoans(List<Loan> loans) {
        this.loans = loans;
    }

    public List<Book> getLoanedBooks() {
        return loanedBooks;
    }

    public void setLoanedBooks(List<Book> loanedBooks) {
        this.loanedBooks = loanedBooks;
    }

    public Loan getLoan() {
        return loan;
    }

    public void setLoan(Loan loan) {
        this.loan = loan;
    }

    public List<Loan> getTheBookLoans() {
        return theBookLoans;
    }

    public void setTheBookLoans(List<Loan> theBookLoans) {
        this.theBookLoans = theBookLoans;
    }

    public String getBookmarkMessage() {
        return bookmarkMessage;
    }

    public void setBookmarkMessage(String bookmarkMessage) {
        this.bookmarkMessage = bookmarkMessage;
    }

    public String getLoanMessage() {
        return loanMessage;
    }

    public void setLoanMessage(String loanMessage) {
        this.loanMessage = loanMessage;
    }

    public List<Review> getPatronReviews() {
        return patronReviews;
    }

    public void setPatronReviews(List<Review> patronReviews) {
        this.patronReviews = patronReviews;
    }

    public String getReviewMessage() {
        return reviewMessage;
    }

    public void setReviewMessage(String reviewMessage) {
        this.reviewMessage = reviewMessage;
    }
    
    public String showUserProfile() {
        FacesContext fc = FacesContext.getCurrentInstance();
        LoginController loginController = (LoginController) fc.getELContext().getELResolver().getValue(fc.getELContext(), null, "loginController");
        patron = loginController.getLoginedPatron();
        loans = new ArrayList<Loan>();
        loans = patron.getLoans();
        
        loanedBooks = new ArrayList<Book>();
        for(int i = 0; i < loans.size(); i ++) {
            book = loans.get(i).getBook();
            loanedBooks.add(book);
        }
        
        markedBooks = new ArrayList<Book>();
        markedBooks = patron.getMarkedBooks();
        patronReviews = new ArrayList<Review>();
        patronReviews = patron.getReviews();
        if(markedBooks.isEmpty()) {
            bookmarkMessage = "You have no bookmarks.";
        }
        if(loanedBooks.isEmpty()) {
            loanMessage = "You have no loan history.";
        }
        if(patronReviews.isEmpty()) {
            reviewMessage = "You didn't leave any comments.";
        }
        return "userProfile.xhtml";
    }
    
    public void returnBook() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String isbn = params.get("isbn");
        String date = params.get("date");
        System.out.println(date + "-----------------------------");

        book = new Book();
        for(int i = 0; i < loanedBooks.size(); i++) {
            if(loanedBooks.get(i).getIsbn().equals(isbn)) {
                book = loanedBooks.get(i);
                book.setAvailability(true);
                bookService.addBook(book);
                theBookLoans = book.getLoans();
                for(int a = 0; a < theBookLoans.size(); a++) {
                    if(theBookLoans.get(a).getLoanDate().equals(date)) {
                        loan = theBookLoans.get(a);
                        loan.setStatus("Completed");
                        bookService.addLoan(loan);
                    }
                    
                }
            }
        }
    }
}
