/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import monashbook.ejb.BookService;
import monashbook.models.Book;
import monashbook.models.Category;
import monashbook.models.Groups;
import monashbook.models.Patron;
import monashbook.models.UserGroup;
import monashbook.models.Users;
import org.joda.time.Days;
import org.joda.time.LocalDate;

/**
 *
 * @author Liang
 */
@Named(value = "indexController")
@ApplicationScoped
public class IndexController {

    @EJB
    BookService bookService;
    private String pageTitle;
    private String searchQuery;
    private List<Book> books;
    private List<Book> categoryBooks;
    private List<Book> searchedBooks;
    private List<Category> categories;
    private String searchMessage;
    private String categoryPageTitle;
    @PostConstruct
    public void init(){
        pageTitle = "Monash Books";
        // Create dummy Patron
        Patron patron1 = new Patron();
        patron1.setEmail("kingso.bboy@gmail.com");
        patron1.setPassword("8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92");
        patron1.setName("Leon");
        patron1.setPhone("0452507625");
        
        // Add the patron to the database
        bookService.addPatron(patron1);
        
        // Create dummy AdminUser
        Users adminUser = new Users();
        adminUser.setUsername("admin");
        adminUser.setPassword("8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918");
        // Create dummy group
        Groups adminGroup = new Groups();
        adminGroup.setGroupname("Admin Group");
        // Create dummy UserGroup
        UserGroup userGroup = new UserGroup();
        userGroup.setUsername(adminUser.getUsername());
        userGroup.setGroupname(adminGroup.getGroupname());
        userGroup.setGroups(adminGroup);
        userGroup.setUsers(adminUser);
        // Ensure entities are stored in each side of the relationship
        adminUser.getUserGroups().add(userGroup);
        adminGroup.getUserGroups().add(userGroup);
        // Add the user and the group to database
        bookService.addAdminUser(adminUser);
        
        // Create dummy books
        // First Book
        Book book1 = new Book();
        book1.setTitle("Thank You for Your Service");
        book1.setAuthor("David Finkel");
        book1.setPublisher("Sarah Crichton Books");
        String pubDate1 = "01/10/2013";
        book1.setPublishDate(pubDate1);
        book1.setIsbn("978-0374180669");
        book1.setDescription("The wars of the past decade have been covered by brave and talented reporters, but none has "
                + "reckoned with the psychology of these wars as intimately as the Pulitzer Prize-winning journalist David Finkel.");
        book1.setImageUrl("/MonashBook-war/resources/img/book1.jpg");
        book1.setAvailability(true);
        
        // Second Book
        Book book2 = new Book();
        book2.setTitle("Humans of New York");
        book2.setAuthor("Brandon Stanton");
        book2.setPublisher("St. Martin's Press");
        String pubDate2 = "15/10/2013";
        book2.setPublishDate(pubDate2);
        book2.setIsbn("978-1250038821");
        book2.setDescription("In the summer of 2010, photographer Brandon Stanton set out on an ambitious project: to "
                + "single-handedly create a photographic census of New York City.  Armed with his camera, he began ");
        book2.setImageUrl("/MonashBook-war/resources/img/book2.jpg");
        book2.setAvailability(true);

        // Third Book
        Book book3 = new Book();
        book3.setTitle("The Night Guest: A Novel");
        book3.setAuthor("Fiona McFarlane");
        book3.setPublisher("Faber & Faber");
        String pubDate3 = "01/10/2013";
        book3.setPublishDate(pubDate3);
        book3.setIsbn("978-0865477735");
        book3.setDescription("Ruth is widowed, her sons are grown, and she lives in an isolated beach house "
                + "outside of town. Her routines are few and small. One day a stranger arrives at her door, looking as "
                + "if she has been blown in from the sea.");
        book3.setImageUrl("/MonashBook-war/resources/img/book3.jpg");
        book3.setAvailability(true);
        
        // Forth Book
        Book book4 = new Book();
        book4.setTitle("Allegiant (Divergent Trilogy)");
        book4.setAuthor("Veronica Roth");
        book4.setPublisher("Katherine Tegen Books");
        String pubDate4 = "22/10/2013";
        book4.setPublishDate(pubDate4);
        book4.setIsbn("978-0062024060");
        book4.setDescription("What if love and loyalty made you do things you never expected? Veronica Roth's Divergent trilogy "
                + "reveals the secrets of the dystopian world that has captivated millions of readers in Divergent and Insurgent");
        book4.setImageUrl("/MonashBook-war/resources/img/book4.jpg");
        book4.setAvailability(true);
        
        // Fifth Book
        Book book5 = new Book();
        book5.setTitle("Doctor Sleep: A Novel");
        book5.setAuthor("Stephen King");
        book5.setPublisher("Scribner");
        String pubDate5 = "24/09/2013";
        book5.setPublishDate(pubDate5);
        book5.setIsbn("978-1476727653");
        book5.setDescription("The Shining, in this instantly riveting novel about the now middle-aged Dan Torrance and the very special "
                + "twelve-year-old girl he must save from a tribe of murderous paranormals. ");
        book5.setImageUrl("/MonashBook-war/resources/img/book5.jpg");
        book5.setAvailability(true);
        
        // Sixth Book
        Book book6 = new Book();
        book6.setTitle("Divergent");
        book6.setAuthor("Veronica Roth");
        book6.setPublisher("Katherine Tegen Books");
        String pubDate6 = "28/02/2012";
        book6.setPublishDate(pubDate6);
        book6.setIsbn("978-0062024039");
        book6.setDescription("In Beatrice Prior’s world, society is divided into five factions. On an appointed day of "
                + "every year, all sixteen-year-olds must select the faction to which they will devote the rest of their lives.");
        book6.setImageUrl("/MonashBook-war/resources/img/book6.jpg");
        book6.setAvailability(true);
        
        // Seventh Book
        Book book7 = new Book();
        book7.setTitle("Grain Brain");
        book7.setAuthor("David Perlmutter");
        book7.setPublisher("Little, Brown and Company");
        String pubDate7 = "17/09/2013";
        book7.setPublishDate(pubDate7);
        book7.setIsbn("978-0316234801");
        book7.setDescription("The devastating truth about the effects of wheat, sugar, and carbs on the brain, with "
                + "a 30-day plan to achieve optimum health.");
        book7.setImageUrl("/MonashBook-war/resources/img/book7.jpg");
        book7.setAvailability(true);
        
        // Eighth Book
        Book book8 = new Book();
        book8.setTitle("Size 12 Is Not Fat: A Heather Wells Mystery");
        book8.setAuthor("Meg Cabot");
        book8.setPublisher("William Morrow Paperbacks");
        String pubDate8 = "27/12/2012";
        book8.setPublishDate(pubDate8);
        book8.setIsbn("978-0060525118");
        book8.setDescription("That was before she left the pop-idol life behind after she gained a "
                + "dress size or two. Now that the glamour and glory days of endless mall appearances are in "
                + "the past, Heather's perfectly happy with her new size 12 shape");
        book8.setImageUrl("/MonashBook-war/resources/img/book8.jpg");
        book8.setAvailability(true);
        
        // Ninth Book
        Book book9 = new Book();
        book9.setTitle("Cache a Predator");
        book9.setAuthor("M. Weidenbenner");
        book9.setPublisher("CreateSpace Independent Publishing Platform");
        String pubDate9 = "28/07/2013";
        book9.setPublishDate(pubDate9);
        book9.setIsbn("978-1490936390");
        book9.setDescription("Officer Brett Reed will do anything to gain custody of his five-year-old daughter, Quinn. "
                + "But when a judge grants Brett's drug-addicted ex-wife custody and slaps him with a protective order for "
                + "losing his temper, he fears for Quinn's safety.");
        book9.setImageUrl("/MonashBook-war/resources/img/book9.jpg");
        book9.setAvailability(true);
        
        // Tehth Book
        Book book10 = new Book();
        book10.setTitle("The Son");
        book10.setAuthor("Philipp Meyer");
        book10.setPublisher("Ecco");
        String pubDate10 = "28/05/2013";
        book10.setPublishDate(pubDate10);
        book10.setIsbn("978-0062120397");
        book10.setDescription("The Son: an epic of the merican West and a multigenerational saga of power, blood, land, and oil that follows the rise of one "
                + "unforgettable Texas family, from the Comanche raids of the 1800s to the to the oil booms of the 20th century.");
        book10.setImageUrl("/MonashBook-war/resources/img/book10.jpg");
        book10.setAvailability(true);
        
        // Eleventh Book
        Book book11 = new Book();
        book11.setTitle("Eleanor & Park");
        book11.setAuthor("Rainbow Rowell");
        book11.setPublisher("St. Martin's Griffin");
        String pubDate11 = "26/02/2013";
        book11.setPublishDate(pubDate11);
        book11.setIsbn("978-1250012579");
        book11.setDescription("Set over the course of one school year in 1986, but brave and desperate enough to try. When Eleanor meets Park, "
                + "you’ll remember your own first love—and just how hard it pulled you under.");
        book11.setImageUrl("/MonashBook-war/resources/img/book11.jpg");
        book11.setAvailability(true);
        
        // Twelfth Book
        Book book12 = new Book();
        book12.setTitle("Gulp: Adventures on the Alimentary Canal");
        book12.setAuthor("Mary Roach");
        book12.setPublisher("W. W. Norton & Company");
        String pubDate12 = "01/04/2013";
        book12.setPublishDate(pubDate12);
        book12.setIsbn("978-0393081572");
        book12.setDescription("The alimentary canal is classic Mary Roach terrain: the questions in their way, "
                + "as the cadavers in Stiff and every bit as surreal as the universe of zero gravity explored in Packing for Mars.");
        book12.setImageUrl("/MonashBook-war/resources/img/book12.jpg");
        book12.setAvailability(true);
        
        // Thirteenth Book
        Book book13 = new Book();
        book13.setTitle("The Guns at Last Light");
        book13.setAuthor("Rick Atkinson");
        book13.setPublisher("Henry Holt and Co.");
        String pubDate13 = "14/05/2013";
        book13.setPublishDate(pubDate13);
        book13.setIsbn("978-0805062908");
        book13.setDescription("It is the twentieth century’s unrivaled epic: at a staggering price, the United States and its allies liberated "
                + "Europe and vanquished Hitler. In the first two volumes of his bestselling Liberation Trilogy");
        book13.setImageUrl("/MonashBook-war/resources/img/book13.jpg");
        book13.setAvailability(true);
        
        // Fourteenth Book
        Book book14 = new Book();
        book14.setTitle("The Ocean at the End of the Lane: A Novel");
        book14.setAuthor("Neil Gaiman");
        book14.setPublisher("Neil Gaiman");
        String pubDate14 = "18/06/2013";
        book14.setPublishDate(pubDate14);
        book14.setIsbn("978-0062255655");
        book14.setDescription("A brilliantly imaginative and poignant fairy tale from the modern master of wonder and terror, The Ocean at the End of the "
                + "Lane is Neil Gaiman’s first new novel for adults");
        book14.setImageUrl("/MonashBook-war/resources/img/book14.jpg");
        book14.setAvailability(true);
        
        // Fifteenth Book
        Book book15 = new Book();
        book15.setTitle("The Round House");
        book15.setAuthor("Louise Erdrich");
        book15.setPublisher("Harper");
        String pubDate15 = "02/10/2012";
        book15.setPublishDate(pubDate15);
        book15.setIsbn("978-0062065247");
        book15.setDescription("Native-American life—Louise Erdrich returns to the territory of her bestselling, Pulitzer Prize "
                + "finalist The Plague of Doves with The Round House, transporting readers to the Ojibwe reservation in North Dakota. ");
        book15.setImageUrl("/MonashBook-war/resources/img/book15.jpg");
        book15.setAvailability(true);
        
        // Create dummy Categories
        Category category1 = new Category();
        category1.setCategory("Social Sciences");
        Category category2 = new Category();
        category2.setCategory("History");
        Category category3 = new Category();
        category3.setCategory("Fiction");
        Category category4 = new Category();
        category4.setCategory("Love and Romance");
        Category category5 = new Category();
        category5.setCategory("Science");
        
        // Add the categories to the database
        bookService.addCategory(category1);
        bookService.addCategory(category2);
        bookService.addCategory(category3);
        bookService.addCategory(category4);
        bookService.addCategory(category5);
        
        // Get the categories from the database and add them to the category list of each book
        Category category1FromDb = bookService.getAllCategories().get(0);
        Category category2FromDb = bookService.getAllCategories().get(1);
        Category category3FromDb = bookService.getAllCategories().get(2);
        Category category4FromDb = bookService.getAllCategories().get(3);
        Category category5FromDb = bookService.getAllCategories().get(4);
        
        book1.getCategories().add(bookService.getAllCategories().get(1));
        book2.getCategories().add(bookService.getAllCategories().get(0));
        book3.getCategories().add(bookService.getAllCategories().get(2));
        book4.getCategories().add(bookService.getAllCategories().get(3));
        book4.getCategories().add(bookService.getAllCategories().get(4));
        book5.getCategories().add(bookService.getAllCategories().get(2));
        book6.getCategories().add(bookService.getAllCategories().get(3));
        book6.getCategories().add(bookService.getAllCategories().get(4));
        book7.getCategories().add(bookService.getAllCategories().get(4));
        book8.getCategories().add(bookService.getAllCategories().get(2));
        book8.getCategories().add(bookService.getAllCategories().get(3));
        book9.getCategories().add(bookService.getAllCategories().get(1));
        book9.getCategories().add(bookService.getAllCategories().get(2));
        book10.getCategories().add(bookService.getAllCategories().get(2));
        book11.getCategories().add(bookService.getAllCategories().get(3));
        book12.getCategories().add(bookService.getAllCategories().get(0));
        book13.getCategories().add(bookService.getAllCategories().get(2));
        book13.getCategories().add(bookService.getAllCategories().get(3));
        book14.getCategories().add(bookService.getAllCategories().get(1));
        book14.getCategories().add(bookService.getAllCategories().get(2));
        book14.getCategories().add(bookService.getAllCategories().get(3));
        book15.getCategories().add(bookService.getAllCategories().get(2));
        
        
        // Ensure entities are stored in each side of the relationship
        
        // Add books to the database
        bookService.addBook(book1);
        bookService.addBook(book2);
        bookService.addBook(book3);
        bookService.addBook(book4);
        bookService.addBook(book5);
        bookService.addBook(book6);
        bookService.addBook(book7);
        bookService.addBook(book8);
        bookService.addBook(book9);
        bookService.addBook(book10);
        bookService.addBook(book11);
        bookService.addBook(book12);
        bookService.addBook(book13);
        bookService.addBook(book14);
        bookService.addBook(book15);
        
        // Get the books from the database and add them to the book list of each category
        category1FromDb.getBooks().add(bookService.getAllBooks().get(1));
        category1FromDb.getBooks().add(bookService.getAllBooks().get(1));
        
        category2FromDb.getBooks().add(bookService.getAllBooks().get(0));
        category2FromDb.getBooks().add(bookService.getAllBooks().get(8));
        category2FromDb.getBooks().add(bookService.getAllBooks().get(13));
        
        category3FromDb.getBooks().add(bookService.getAllBooks().get(2));
        category3FromDb.getBooks().add(bookService.getAllBooks().get(4));
        category3FromDb.getBooks().add(bookService.getAllBooks().get(7));
        category3FromDb.getBooks().add(bookService.getAllBooks().get(8));
        category3FromDb.getBooks().add(bookService.getAllBooks().get(9));
        category3FromDb.getBooks().add(bookService.getAllBooks().get(12));
        category3FromDb.getBooks().add(bookService.getAllBooks().get(13));
        category3FromDb.getBooks().add(bookService.getAllBooks().get(14));
        
        category4FromDb.getBooks().add(bookService.getAllBooks().get(3));
        category4FromDb.getBooks().add(bookService.getAllBooks().get(5));
        category4FromDb.getBooks().add(bookService.getAllBooks().get(7));
        category4FromDb.getBooks().add(bookService.getAllBooks().get(10));
        category4FromDb.getBooks().add(bookService.getAllBooks().get(12));
        category4FromDb.getBooks().add(bookService.getAllBooks().get(13));

        category5FromDb.getBooks().add(bookService.getAllBooks().get(3));
        category5FromDb.getBooks().add(bookService.getAllBooks().get(5));
        category5FromDb.getBooks().add(bookService.getAllBooks().get(6));
        
        // Update the categories in the database
        bookService.addCategory(category1FromDb);
        bookService.addCategory(category2FromDb);
        bookService.addCategory(category3FromDb);
        bookService.addCategory(category4FromDb);
        bookService.addCategory(category5FromDb);
    }
    public String getPageTitle() {
        return pageTitle;
    }
    
    public List<Book> getBooks() {
        return bookService.getAllBooks();
    }

    public List<Book> getCategoryBooks() {
        return categoryBooks;
    }

    public void setCategoryBooks(List<Book> categoryBooks) {
        this.categoryBooks = categoryBooks;
    }
        public String getSearchQuery() {
        return searchQuery;
    }

    public void setSearchQuery(String searchQuery) {
        this.searchQuery = searchQuery;
    }

    public String getSearchMessage() {
        return searchMessage;
    }

    public void setSearchMessage(String searchMessage) {
        this.searchMessage = searchMessage;
    }

    public String getCategoryPageTitle() {
        return categoryPageTitle;
    }

    public void setCategoryPageTitle(String categoryPageTitle) {
        this.categoryPageTitle = categoryPageTitle;
    }
    
    public List<Book> getLatestBooks() {
        List<Book> latestBooks = new ArrayList<Book>();
        books = bookService.getAllBooks();
        System.out.println("size size size size size" + books.size());
        try{
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            Date date = new Date();
            String sysDate = dateFormat.format(date);
            Date currentDate = dateFormat.parse(sysDate);
            LocalDate nowDate = LocalDate.fromDateFields(currentDate);
            for(Book b: books) {
                System.out.println(b.getTitle());
                String string = b.getPublishDate();
                Date pubDate = dateFormat.parse(string);
                LocalDate bookDate = LocalDate.fromDateFields(pubDate);
                int numOfDays = Days.daysBetween(bookDate, nowDate).getDays();
                
                if(numOfDays < 50){
                    latestBooks.add(b);
                }
            }
        }
        catch(Exception e) {
            System.out.println("Invalid date format.");
        }
        return latestBooks;
    }
    
    public List<Book> getSearchedBooks() {
        searchedBooks = new ArrayList<Book>();
        for(Book b: books) {
            if(b.getTitle().toLowerCase().trim().contains(searchQuery.toLowerCase().trim())){
                searchedBooks.add(b);
            }
            else if(b.getAuthor().toLowerCase().trim().contains(searchQuery.toLowerCase().trim())){
                searchedBooks.add(b);
            }
            else if(b.getIsbn().toLowerCase().trim().contains(searchQuery.toLowerCase().trim())){
                searchedBooks.add(b);
            }else {
                searchMessage = "No matching books.";
            }
        }
        return searchedBooks;
        
    }
    
    public String showCategoryBooks() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
         categories = new ArrayList<Category>();
         categories = bookService.getAllCategories();
         String category = params.get("category");
         categoryPageTitle = category;
         Category bookCategory = new Category();
         for(int i = 0; i < categories.size(); i++) {
             System.out.println(categories.get(i).getCategory());
             
             if(categories.get(i).getCategory().equals(category)) {
                 bookCategory = categories.get(i);
                 categoryBooks = bookCategory.getBooks();
             }
         }
         return "category.xhtml";
         
    }
    

    
}
