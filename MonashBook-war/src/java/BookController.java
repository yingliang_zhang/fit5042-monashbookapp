/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import monashbook.ejb.BookService;
import monashbook.models.Book;
import monashbook.models.Patron;
import monashbook.models.Review;

/**
 *
 * @author Liang
 */
@Named(value = "bookController")
@SessionScoped
public class BookController implements Serializable {

    @EJB
    BookService bookService;
    private List<Book> books;
    private List<Book> theDetailBook;
    private Book book;
    private Patron patron;
    private String comments;
    private List<Review> bookReviews;
    private int rating;
    
    public BookController() {
        
    }
    
    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
    
    public String read(Book book) {
        this.book = book;
        return "book.xhtml";
    }
    
    public String setupCreate() {
        book = new Book();
        return "book.xhtml";
    }
    
    public String create() {
        bookService.addBook(book);
        return "index.xhtml";
    }
    
    public String update() {
        bookService.updateBook(book);
        return "index.xhtml";
    }
    
    public String delete(Book book) {
        bookService.deleteBook(book);
        return "index.xhtml";
    }

    public List<Book> getTheDetailBook() {
        return theDetailBook;
    }
    
    public List<Book> getAllBooks() {
        return bookService.getAllBooks();
    }

    public void setTheDetailBook(List<Book> theDetailBook) {
        this.theDetailBook = theDetailBook;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public List<Review> getBookReviews() {
        bookReviews = book.getReviews();
        return bookReviews;
    }

    public void setBookReviews(List<Review> bookReviews) {
        this.bookReviews = bookReviews;
    }
    
    
    public String showBook() {
        FacesContext fc = FacesContext.getCurrentInstance();
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        theDetailBook = new ArrayList<Book>();
        String isbn = params.get("isbn");
        books = new ArrayList<>();
        book = new Book();
        books = bookService.getAllBooks();
        for(int i = 0; i < books.size(); i++) {
            if(books.get(i).getIsbn().equals(isbn)) {
                book = books.get(i);
                theDetailBook.add(book);
            }
        }
        return "book.xhtml";
    }
    
    public void postComments() {
        FacesContext fc = FacesContext.getCurrentInstance();
        LoginController loginController = (LoginController) fc.getELContext().getELResolver().getValue(fc.getELContext(), null, "loginController");
        Map<String, String> params = fc.getExternalContext().getRequestParameterMap();
        String isbn = params.get("isbn");
        theDetailBook = new ArrayList<Book>();
        books = new ArrayList<>();
        book = new Book();
        books = bookService.getAllBooks();
        for(int i = 0; i < books.size(); i++) {
            if(books.get(i).getIsbn().equals(isbn)) {
                book = books.get(i);
                theDetailBook.add(book);
            }
        }
        patron = loginController.getLoginedPatron();
        if(patron == null) {
            loginController.setErrorMessage("Please login to make comments.");
        }else {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
            Date date = new Date();
            String currentDate = dateFormat.format(date);
            Review review = new Review();
            review.setPatron(patron);
            review.setBook(book);
            review.setCommentDate(currentDate);
            review.setComment(comments);
            review.setRating(rating);
            
            book.getReviews().add(review);
            loginController.getPatronReviews().add(review);
            bookService.addBook(book);
            //bookService.addReview(review);
   
        }
    }
    
}
