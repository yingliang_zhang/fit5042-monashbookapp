/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monashbook.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 *
 * @author Liang
 */
@Entity
public class Patron implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="PATRON_ID")
    private Long id;
    @NotNull
    private String email;
    @NotNull
    private String name;
    @NotNull
    private String phone;
    @NotNull
    private String password;
    @ManyToMany
    @JoinTable(name = "BOOK_MARK",
               joinColumns = @JoinColumn(name = "patron_fk"),
               inverseJoinColumns = @JoinColumn(name = "book_fk"))
    private List<Book> markedBooks = new ArrayList<>();
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy = "patron")
    private List<Loan> loans = new ArrayList<Loan>();
    
    @OneToMany(cascade=CascadeType.ALL, mappedBy = "patron")
    private List<Review> reviews = new ArrayList<Review>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Loan> getLoans() {
        return loans;
    }

    public void setLoans(List<Loan> loans) {
        this.loans = loans;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public List<Book> getMarkedBooks() {
        return markedBooks;
    }

    public void setMarkedBooks(List<Book> books) {
        this.markedBooks = books;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Patron)) {
            return false;
        }
        Patron other = (Patron) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "monashbook.models.Patron[ id=" + id + " ]";
    }
    
}
