/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package monashbook.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import monashbook.models.Author;
import monashbook.models.Book;
import monashbook.models.Category;
import monashbook.models.Groups;
import monashbook.models.Loan;
import monashbook.models.Patron;
import monashbook.models.Review;
import monashbook.models.Users;

/**
 *
 * @author Liang
 */
@Stateless
@LocalBean
public class BookService {
    @PersistenceContext(unitName = "MonashBook-ejbPU")
    private EntityManager em;
    public void addBook(Book b) {
        em.merge(b);
    }
    
    public void deleteBook(Book b ) {
        em.remove(em.merge(b));
    }
    
    public void updateBook(Book b) {
        em.merge(b);
    }
    
    public void addPatron(Patron p) {
        em.merge(p);
    }
    
    public void addAuthor(Author a) {
        em.merge(a);
    }
    
    public void addCategory(Category c) {
        em.merge(c);
    }
    
    public void addLoan(Loan l) {
        em.merge(l);
    }
    
    public void addAdminUser(Users u) {
        em.merge(u);
    }
    
    public void addGroup(Groups g) {
        em.merge(g);
    }
    
    public void addReview(Review r) {
        em.merge(r);
    }
    
    public List<Patron> getAllPatrons() {
        TypedQuery<Patron> q = em.createQuery("SELECT p FROM Patron p", Patron.class);
        return q.getResultList();
    }
    
    public List<Book> getAllBooks() {
        TypedQuery<Book> q = em.createQuery("SELECT b FROM Book b", Book.class);
        return q.getResultList();
    }
    
    public List<Author> getAllAuthors() {
        TypedQuery<Author> q = em.createQuery("SELECT a FROM Author a", Author.class);
        return q.getResultList();
    }
    
    public List<Category> getAllCategories() {
        TypedQuery<Category> q = em.createQuery("SELECT c FROM Category c", Category.class);
        return q.getResultList();
    }

    public void persist(Object object) {
        em.persist(object);
    }

}
