# FIT5042: Enterprise application development for the web
  Assignment Two: Monash Books online system
---------------------------------------------------------
Functionality outline:
- Homepage showing lastest books with good styling
- Book category pages showing different category of books
- User registration and authentication
- User can make book loans, return books, bookmark books,
  search for books and post comments
- Privare user profile page showing user's loan history
  bookmarks and overview of any comments.
- Administration for viewing, deleting, adding and editing
  book details and comments.
- Overall input validation
